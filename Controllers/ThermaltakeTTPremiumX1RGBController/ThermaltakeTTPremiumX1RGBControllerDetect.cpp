#include <hidapi/hidapi.h>
#include "Detector.h"
#include "RGBController.h"

#include "ThermaltakeTTPremiumX1RGBController.h"
#include "RGBController_ThermaltakeTTPremiumX1RGB.h"
#define ThermaltakeTTPremiumX1RGB_DETECTOR_NAME "Thermaltake TT Premium X1 RGB Detector"

#define THERMALTAKE_VID                     0x264A
#define THERMALTAKE_TT_PREMIUM_X1_RGB_PID   0x3013

void DetectThermaltakeTTPremiumX1RGBControllers(hid_device_info* info, const std::string& name)
{
    hid_device* dev = hid_open_path(info->path);
    if( dev )
    {
        ThermaltakeTTPremiumX1RGBController* controller = new ThermaltakeTTPremiumX1RGBController(dev, info->path);
        RGBController_ThermaltakeTTPremiumX1RGB* rgb_controller = new RGBController_ThermaltakeTTPremiumX1RGB(controller);
        rgb_controller->name = name;
        ResourceManager::get()->RegisterRGBController(rgb_controller);
    }
}

REGISTER_HID_DETECTOR_IPU("Thermaltake TT Premium X1 RGB", DetectThermaltakeTTPremiumX1RGBControllers, THERMALTAKE_VID, THERMALTAKE_TT_PREMIUM_X1_RGB_PID, 1, 0xFF00, 1);
