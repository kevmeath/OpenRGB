#include "RGBController.h"

#include <string>
#include <hidapi/hidapi.h>

#pragma once

class ThermaltakeTTPremiumX1RGBController
{
public:
    ThermaltakeTTPremiumX1RGBController(hid_device* dev_handle, const char* path);
    ~ThermaltakeTTPremiumX1RGBController();

    std::string GetDeviceLocation();
    std::string GetSerialString();

private:
    hid_device*             dev;
    std::string             location;
};
