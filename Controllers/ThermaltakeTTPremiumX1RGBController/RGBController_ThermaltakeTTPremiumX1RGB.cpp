#include "RGBController_ThermaltakeTTPremiumX1RGB.h"
#include "ThermaltakeTTPremiumX1RGBController.h"

RGBController_ThermaltakeTTPremiumX1RGB::RGBController_ThermaltakeTTPremiumX1RGB(ThermaltakeTTPremiumX1RGBController* controller)
{
    name        = "Thermaltake TT Premium X1 RGB";
    vendor      = "Thermaltake";
    type        = DEVICE_TYPE_KEYBOARD;
    description = "Thermaltake TT Premium X1 RGB Device";
    location    = controller->GetDeviceLocation();
    serial      = controller->GetSerialString();
}

void RGBController_ThermaltakeTTPremiumX1RGB::SetupZones()
{

}

void RGBController_ThermaltakeTTPremiumX1RGB::ResizeZone(int /*zone*/, int /*new_size*/)
{

}

void RGBController_ThermaltakeTTPremiumX1RGB::DeviceUpdateLEDs()
{

}

void RGBController_ThermaltakeTTPremiumX1RGB::UpdateZoneLEDs(int /*zone*/)
{

}

void RGBController_ThermaltakeTTPremiumX1RGB::UpdateSingleLED(int /*led*/)
{

}

void RGBController_ThermaltakeTTPremiumX1RGB::SetCustomMode()
{

}

void RGBController_ThermaltakeTTPremiumX1RGB::DeviceUpdateMode()
{

}
