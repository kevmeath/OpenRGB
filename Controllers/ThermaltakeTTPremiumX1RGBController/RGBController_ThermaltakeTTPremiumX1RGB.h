#pragma once

#include "RGBController.h"
#include "ThermaltakeTTPremiumX1RGBController.h"

class RGBController_ThermaltakeTTPremiumX1RGB : public RGBController
{
public:
    RGBController_ThermaltakeTTPremiumX1RGB(ThermaltakeTTPremiumX1RGBController*);

    void        SetupZones();

    void        ResizeZone(int zone, int new_size);

    void        DeviceUpdateLEDs();
    void        UpdateZoneLEDs(int zone);
    void        UpdateSingleLED(int led);

    void        SetCustomMode();
    void        DeviceUpdateMode();
};
