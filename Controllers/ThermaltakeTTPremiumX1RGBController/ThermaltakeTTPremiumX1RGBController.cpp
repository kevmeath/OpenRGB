#include "ThermaltakeTTPremiumX1RGBController.h"

#include <cstring>

ThermaltakeTTPremiumX1RGBController::ThermaltakeTTPremiumX1RGBController(hid_device* dev_handle, const char* path)
{
    dev         = dev_handle;
    location    = path;
}

ThermaltakeTTPremiumX1RGBController::~ThermaltakeTTPremiumX1RGBController()
{
    hid_close(dev);
}

std::string ThermaltakeTTPremiumX1RGBController::GetDeviceLocation()
{
    return("HID: " + location);
}

std::string ThermaltakeTTPremiumX1RGBController::GetSerialString()
{
    wchar_t serial_string[128];
    int ret = hid_get_serial_number_string(dev, serial_string, 128);

    if(ret != 0)
    {
        return("");
    }

    std::wstring return_wstring = serial_string;
    std::string return_string(return_wstring.begin(), return_wstring.end());

    return(return_string);
}
